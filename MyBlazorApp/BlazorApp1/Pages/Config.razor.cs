﻿namespace BlazorApp1.Pages
{
    using Microsoft.AspNetCore.Components;
    using Microsoft.Extensions.Configuration;

    public partial class Config
    {
        [Inject]
        public IConfiguration Configuration { get; set; }
    }
}